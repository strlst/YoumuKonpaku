#!/usr/bin/env python3
from youmu import Youmu
from youmu import Player
from youmu import Bestfriend, Choice, Roll, Join, Play, Stop, Exit
from colorama import init, Fore, Back, Style

if __name__ == '__main__':
    # create bot instance
    youmu = Youmu()
    # create player instance
    player = Player(youmu)
    # initialize colorama
    init()
    print(Fore.MAGENTA + Style.BRIGHT)
    # add commands for the bot to use
    youmu.add_command(Bestfriend.create_command(), 'bestfriend')
    youmu.add_command(Choice.create_command(), 'choice')
    youmu.add_command(Roll.create_command(), 'roll')
    # darn timeout errors
    #youmu.add_command(Join.create_command(player), 'join')
    #youmu.add_command(Play.create_command(player), 'play')
    #youmu.add_command(Stop.create_command(player), 'stop')
    #youmu.add_command(Exit.create_command(player), 'exit')
    print(Style.RESET_ALL)
    # finally run bot
    youmu.myon()
