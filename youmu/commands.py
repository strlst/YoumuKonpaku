from .youmu import Youmu
from . import config
from random import randint

### parent structures ###

class Command(object):
    """basic structure of a Command"""
    async def execute(self, bot, message):
        return 'seems like some beginner command aint even know how to override me'

### general commands ###

class Bestfriend(Command):
    """prints the first and foremost master of the config"""
    def __init__(self, master):
        self._master = master

    @classmethod
    def create_command(cls):
        bestfriend = cls(config.masters[0])
        print('bestfriend command created')
        return bestfriend

    async def execute(self, bot, message):
        master = await bot.get_user_info(self._master)
        await bot.send_message(message.channel, '{0} of course!'.format(master.display_name))

class Choice(Command):
    """makes a choice"""
    @classmethod
    def create_command(cls):
        print('choice command created')
        return cls()

    async def execute(self, bot, message):
        command = message.content[message.content.index(' '):].strip()
        choices = command.split(' ')
        if len(choices) < 3 :
            await bot.send_message(message.channel, "What do you want me to choose from? baka")
            return
        await bot.send_message(message.channel, choices[randint(1, len(choices) - 1)])

class Roll(Command):
    """rolls a number between 0 and <numberRange> (100 by default)"""
    @classmethod
    def create_command(cls):
        print('roll command created')
        return cls()

    async def execute(self, bot, message):
        numberRange = 100
        command = message.content[message.content.index(' '):].strip()
        subcommands = command.split(' ')
        if len(subcommands) == 2:
            numberRange = int(subcommands[1])
        await bot.send_message(message.channel, "{0} has rolled {1}".format(message.author.name, str(randint(0, numberRange))))

### player commands ###

class Join(Command):
    """joins the channel the user who used the command is in"""
    def __init__(self, player):
        self.player = player

    @classmethod
    def create_command(cls, player):
        join = cls(player)
        print('join command created')
        return join

    async def execute(self, bot, message):
        await self.player.join(message)

class Play(Command):
    """plays a yt video in the channel is currently in"""
    def __init__(self, player):
        self.player = player

    @classmethod
    def create_command(cls, player):
        play = cls(player)
        print('play command created')
        return play

    async def execute(self, bot, message):
        await self.player.play(message)

class Stop(Command):
    """stops playing"""
    def __init__(self, player):
        self.player = player

    @classmethod
    def create_command(cls, player):
        stop = cls(player)
        print('stop command created')
        return stop

    async def execute(self, bot, message):
        await self.player.stop(message)

class Exit(Command):
    """exits from the channel youmu is currently in"""
    def __init__(self, player):
        self.player = player

    @classmethod
    def create_command(cls, player):
        exit = cls(player)
        print('exit command created')
        return exit

    async def execute(self, bot, message):
        await self.player.exit(message)

### osu commands ###

### weather commands ###
