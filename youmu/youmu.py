import discord
import logging
import asyncio
from . import config
from colorama import init, Fore, Back, Style

class Youmu(discord.Client):

    def __init__(self):
        super().__init__()
        # create store for normal commands
        self._commands = dict()
        # set up logging
        logging.basicConfig(level=logging.INFO)
        # initialize colorama
        init()

    # name run and start were already occupied, so I came up with myon
    def myon(self):
        print(Fore.CYAN + "Initialized successfully! Let's have fun!" + Style.RESET_ALL)
        #super().run(config.token)
        self.run(config.token)

    def add_command(self, command, commandname):
        self._commands[commandname] = command

    def dummy(self):
        print('dummy activated')

    async def on_message(self, message):
        if not self.user in message.mentions:
            return

        command = message.content
        command = command[command.index(' '):len(command)]
        command = command.strip()

        args = command.split(' ')

        # if command is defined in commands dict
        if args[0] in self._commands:
            # access execute method of command object in list
            # corresponding to the command identifier the user specified
            # additionally, give bot and message instance
            await self._commands[args[0]].execute(self, message)

        print(Fore.GREEN + Style.BRIGHT + message.author.name + Style.RESET_ALL + " used " + Fore.CYAN + Style.BRIGHT + command + Fore.RED)

        await self.process_commands(message)
